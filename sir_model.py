import numpy as np
from scipy.integrate import odeint
import matplotlib.pyplot as plt
from data_getter import data

# SIR model generalized
# Applied for italy

lom = data.get_regioni(name="Lombardia")

totale_casi = np.array(lom.totale_attualmente_positivi)[0]
dimessi_guariti = np.array(lom.dimessi_guariti)[0]
deceduti = np.array(lom.deceduti)[0]
totale_poplazione = 100000

# ---------- MODEL PARAMETERS ----------#

# Initial conditions

n = totale_poplazione				# total population

i0 = totale_casi					# init infected
r0 = dimessi_guariti				# init recovered
s0 = n - i0							# init susceptible

beta = 0.33							# contact rate
gamma = 1./7						# recovery rate

time = np.linspace(0, 307,307)		# Time as a evenly spaced numbers (found online)

# --------------------------------------#


# Differential equation for a SIR model
def diff(y, t, n, beta, gamma):

	s, i, r = y
	

	dS = -beta * s * i / n
	dI = beta * s * i / n - gamma * i
	dR = gamma * i

	return dS, dI, dR


# Init
y0 = s0, i0, r0

# Integrate
ret = odeint(diff, y0, time, args=(n, beta, gamma))
s, i, r = ret.T

beta = 0.12	

ret1 = odeint(diff, y0, time, args=(n, beta, gamma))
s1, i1, r1 = ret1.T

#s = np.array(list(s[:28]) + list(s1[126:]))
#i = np.array(list(i[:28]) + list(i1[126:]))
#r = np.array(list(r[:28]) + list(r1[126:]))

#s = np.array(list(map(np.mean, zip(s,s1))))
#i = np.array(list(map(np.mean, zip(i,i1))))
#r = np.array(list(map(np.mean, zip(r,r1))))

# Plot the data
fig = plt.figure(facecolor='w')
ax = fig.add_subplot(111, axisbelow=True)

ax.plot(time, s/totale_poplazione, 'b', label='Susceptible')
ax.plot(time, i/totale_poplazione, 'r', label='Infected')
ax.plot(time, r/totale_poplazione, 'g', label='Recovered with immunity')


ax.plot(np.array(lom.totale_attualmente_positivi)/totale_poplazione, label='Real')

ax.set_xlabel('Days')
ax.set_ylabel('Infected x 10^5)')
ax.set_ylim(0,0.5)

legend = ax.legend()

plt.grid(True)
plt.title('SIR model')

plt.show()