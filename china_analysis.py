from datetime import datetime
import matplotlib.pyplot as plt
import numpy as np
import pandas
import urllib.request
from data_getter import data

# Calculates % of new infected per day
def increment_per_day(df):

	pos = np.array(df.totale_attualmente_positivi)

	incr = [pos[0]]

	for i in range(1, len(pos)):

		incr.append((pos[i] - pos[i-1])*100/pos[i-1])

	return incr


#Gets data from csv
df = pandas.read_csv('data/world_full_data.csv')
pandas.to_datetime(df.date).dt.strftime('%d/%m')

nazione = data.get_nazione()
lombardia = data.get_regioni(name="Lombardia")
piemonte = data.get_regioni(name="Piemonte")

#------------------------------------------------------------- #

# Filter for china
col_name = 'location'
query = "{} == '{}'".format(col_name, 'China')
df = df.query(query)

date = [i for i in range(1,len(df.date)+1)]

plt.plot(date, df.new_cases)
plt.plot(nazione.nuovi_attualmente_positivi)

plt.title("Casi per giorno Italia/Cina")
plt.legend(['cases_per_day_china', 'cases_per_day_italy'])
plt.grid(True)
plt.xticks(rotation=90)

plt.show()

#------------------------------------------------------------- #

# Getting % of new infected

incr = increment_per_day(nazione)
incr1 = increment_per_day(lombardia)
incr2 = increment_per_day(piemonte)

# % of new infected per day in china
tot = np.array(df.total_cases)
incr_cina = [tot[0]]

for i in range(1,len(tot)):
	incr_cina.append((tot[i]-tot[i-1])*100/tot[i-1])

date_it = [i[5:-8] for i in nazione.data]

# Plot results
plt.plot(date_it,incr)
plt.plot(incr1)
plt.plot(incr2)
plt.plot(incr_cina)

plt.title("% incremento per giorno Italia")
plt.xlabel("Data")
plt.ylabel('Percentuale di nuovi positivi')
plt.legend(['Italia', 'Lombardia', 'Piemonte','Cina'])
plt.grid(True)
plt.xticks(rotation=90)

plt.show()

#------------------------------------------------------------- #

# Approx a normal distribution

sigma = np.mean(nazione.nuovi_attualmente_positivi)
mu = np.std(nazione.nuovi_attualmente_positivi)


distr = np.random.normal(sigma, mu, 100000)

count, bins, ignored = plt.hist(distr, 50, density=True)

plt.plot(bins, 1/(sigma * np.sqrt(2 * np.pi)) *
         np.exp( - (bins - mu)**2 / (2 * sigma**2) ),
         linewidth=2, color='r')
plt.plot()


plt.title("Possible gaussian")

plt.show()