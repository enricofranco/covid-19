from datetime import datetime
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import pandas
import urllib.request

URL = 'https://raw.githubusercontent.com/pcm-dpc/COVID-19/master/dati-regioni/dpc-covid19-ita-regioni.csv'
POPOLAZIONE = './data/popolazione.csv'
DATA_QUARANTENA = '2020-03-08'

regioni = pandas.read_csv(URL)
regioni.data = pandas.to_datetime(regioni.data)

popolazione = pandas.read_csv(POPOLAZIONE)

# Filters
col_name = 'denominazione_regione'
name = 'Piemonte'
query = "{} == '{}'".format(col_name, name)
piemonte = regioni.query(query)

name = 'Lombardia'
query = "{} == '{}'".format(col_name, name)
lombardia = regioni.query(query)

# Piemonte vs Lombardia comparison
fig = plt.figure()
ax1 = fig.add_subplot(121)
ax2 = fig.add_subplot(122)

piemonte_plot = piemonte.plot(x = 'data',
    y = ['tamponi', 'totale_attualmente_positivi'],
    label = ['Tamponi', 'Positivi'],
    ax = ax1)
piemonte_plot.set_title('Piemonte')
piemonte_plot.set_xlabel('Data')
piemonte_plot.set_ylabel('Individui')
piemonte_plot.grid(True)
piemonte_plot.axvline(pandas.to_datetime(DATA_QUARANTENA), color='r', linestyle='--', lw=1) 

lombardia_plot = lombardia.plot(x = 'data',
    y = ['tamponi', 'totale_attualmente_positivi'],
    label = ['Tamponi', 'Positivi'],
    ax = ax2)
lombardia_plot.set_title('Lombardia')
lombardia_plot.set_xlabel('Data')
lombardia_plot.set_ylabel('Individui')
lombardia_plot.grid(True)

# Piemonte analysis
abitanti = popolazione.query("regione == 'Piemonte'").popolazione.item()
superficie = popolazione.query("regione == 'Piemonte'").superficie.item()
densita = abitanti / superficie

fig = plt.figure()
ax1 = fig.add_subplot(121)
ax2 = fig.add_subplot(122)
fig.suptitle('Copertura tamponi in Piemonte')
piemonte.insert(len(piemonte.columns), 'tamponi_percentuale', piemonte.tamponi / abitanti * 100.0)
piemonte.insert(len(piemonte.columns), 'infetti_percentuale', piemonte.totale_attualmente_positivi / abitanti * 100.0)
piemonte.insert(len(piemonte.columns), 'tamponi_superficie', piemonte.tamponi / superficie)
pp = piemonte.plot(x = 'data',
    y = ['tamponi_percentuale', 'infetti_percentuale'],
    label = ['Percentuale tamponi', 'Percentuale infetti'],
    ax = ax1,
    grid = True,
    # title = 'Percentuale tamponi'
)
pp.set_xlabel('Data')
pp = piemonte.plot(x = 'data',
    y = ['tamponi_superficie'],
    label = ['Tamponi al $km^2$'],
    ax = ax2,
    grid = True,
    # title = 'Tamponi su superficie'
)
pp.set_xlabel('Data')

plt.show()