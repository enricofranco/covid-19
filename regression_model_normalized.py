from data_getter import data
import matplotlib.pyplot as plt
from sklearn import linear_model
from sklearn.ensemble import AdaBoostRegressor
from sklearn.metrics import mean_squared_error
from sklearn.metrics import mean_absolute_error
import numpy as np
import pandas

POPOLAZIONE = './data/popolazione.csv'

def prepare_data(df):

	t = np.array(df.tamponi)
	per_day = [t[0]]

	for i in range(1,len(t)):
		per_day.append(t[i] - t[i-1])

	x = [list(i) for i in zip(range(1, len(df.data)+1), df.tamponi, per_day)]
	y = [[i] for i in df.totale_positivi]

	return x,y

def prepare_model(x, y):

	regr = AdaBoostRegressor(loss='exponential', learning_rate=0.1, n_estimators=200)
	regr.fit(x, y)

	print("\n--- MODEL SCORES ---")
	print("MSE --> " + str(mean_squared_error(y, regr.predict(x))))
	print("MAE --> " + str(mean_absolute_error(y, regr.predict(x))))
	
	return regr

nazione = data.get_nazione()
piemonte = data.get_regioni(name="Piemonte")
lombardia = data.get_regioni(name="Lombardia")

# print(piemonte.head())

# Finding density
popolazione = pandas.read_csv(POPOLAZIONE)
abitanti = popolazione.query("regione == 'Piemonte'").popolazione.item()
superficie = popolazione.query("regione == 'Piemonte'").superficie.item()

x1,y1 = prepare_data(piemonte)
x2,y2 = prepare_data(lombardia)
x3,y3 = prepare_data(nazione)

regr1 = prepare_model(x1,y1)
regr2 = prepare_model(x2,y2)
regr3 = prepare_model(x3,y3)

date = [i[5:-9] for i in piemonte.data]

# Plot results
plt.plot(date, regr1.predict(x1))
plt.plot(y1)

plt.plot(regr2.predict(x2))
plt.plot(y2)

plt.plot(regr3.predict(x3))
plt.plot(y3)


plt.xlabel("Giorni")
plt.ylabel('Positivi')
plt.grid(True)

plt.legend(['Previsti Piemonte', 'Real Piemonte', 'Previsti Lombardia', 
			'Real Lombardia', 'Previsti nazione', 'Real nazione'])

plt.title("Modelli di regressione")

plt.show()