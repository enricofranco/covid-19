from datetime import datetime
import matplotlib.pyplot as plt
import pandas
import urllib.request

URL = 'https://raw.githubusercontent.com/pcm-dpc/COVID-19/master/dati-andamento-nazionale/dpc-covid19-ita-andamento-nazionale.csv'

df = pandas.read_csv(URL)
df.data = pandas.to_datetime(df.data).dt.strftime('%d/%m') # Convert dates for readability
andamento_nazionale_plot = df.plot(x = 'data',
    y = ['ricoverati_con_sintomi', 'terapia_intensiva', 'deceduti', 'totale_casi'],
    label = ['Ricoverati con sintomi', 'Terapia intensiva', 'Deceduti', 'Totale casi'])
andamento_nazionale_plot.set_title('Contagio COVID19')
andamento_nazionale_plot.set_xlabel('Data')
andamento_nazionale_plot.set_ylabel('Individui')
andamento_nazionale_plot.grid(True)

df.insert(len(df.columns), 'letalità', df.deceduti / df.totale_casi * 100.0)
df.insert(len(df.columns), 'perc_dimissioni', df.dimessi_guariti / df.totale_casi * 100.0)
perc_nazionale_plot = df.plot(x = 'data',
    y = ['letalità', 'perc_dimissioni'],
    label = ['Letalità %', 'Guarigioni %'])
perc_nazionale_plot.set_title('Contagio COVID19')
perc_nazionale_plot.set_xlabel('Data')
perc_nazionale_plot.set_ylabel('Percentuale')
perc_nazionale_plot.grid(True)

plt.show()