from datetime import datetime
import matplotlib.pyplot as plt
import pandas
import urllib.request

# Each method returns a pandas data frame

class data:

	url = 'https://raw.githubusercontent.com/pcm-dpc/COVID-19/master/'
	nazionale = 'dati-andamento-nazionale/dpc-covid19-ita-andamento-nazionale.csv'
	regione = 'dati-regioni/dpc-covid19-ita-regioni.csv'
	province = 'dati-province/dpc-covid19-ita-province.csv'

	def from_to(self, df, begin, end):
		''' Returns the data in between the dates
			 specified in the params - example'''

		col_name = 'data'
		query = "{} > '{}' and {} < '{}'".format(col_name, begin, col_name, end)
		df = df.query(query)

		return df

	def get_nazione(begin="all", end="all"):
		''' Returns all data at a national level 
			begin = start date
			end = end date '''

		to_get = data.url + data.nazionale
		
		df = pandas.read_csv(to_get)
		pandas.to_datetime(df.data).dt.strftime('%d/%m')

		if (begin != "all"):
			col_name = 'data'
			query = "{} > '{}' and {} < '{}'".format(col_name, begin, col_name, end)
			df = df.query(query)

		return df

	def get_regioni(begin="all", end="all", name="all"):
		''' Returns the data at a regional level
			name = region name - Default "all" 
			begin = start date 
			end = end date '''

		to_get = data.url + data.regione
		
		df = pandas.read_csv(to_get)
		pandas.to_datetime(df.data).dt.strftime('%d/%m')

		if (name != "all"):
			col_name = 'denominazione_regione'
			query = "{} == '{}'".format(col_name, name)
			df = df.query(query)

		if (begin != "all"):
			col_name = 'data'
			query = "{} > '{}' and {} < '{}'".format(col_name, begin, col_name, end)
			df = df.query(query)

		return df

	def get_province(begin="all", end="all", name="all"):
		''' Returns the data at a province level
			name = province name - Default "all" 
			begin = start date 
			end = end date '''

		to_get = data.url + data.province
		
		df = pandas.read_csv(to_get)
		pandas.to_datetime(df.data).dt.strftime('%d/%m')

		if (name != "all"):
			col_name = 'denominazione_provincia'
			query = "{} == '{}'".format(col_name, name)
			df = df.query(query)

		if (begin != "all"):
			col_name = 'data'
			query = "{} > '{}' and {} < '{}'".format(col_name, begin, col_name, end)
			df = df.query(query)

		return df
