# Acquisizione dei dati

- `popolazione.json` e `popolazione.csv`. Dati aggiornati al 1 gennaio 2019, fonte ISTAT. [https://it.wikipedia.org/wiki/Regioni_d%27Italia#Dati_demografici_e_geografici](https://it.wikipedia.org/wiki/Regioni_d%27Italia#Dati_demografici_e_geografici)

  > Per le province autonome di Trento e Bolzano il dato è aggiornato al 30 giugno 2019, fonte ISTAT.
  > - [https://it.wikipedia.org/wiki/Provincia_autonoma_di_Trento](https://it.wikipedia.org/wiki/Provincia_autonoma_di_Trento)
  > - [https://it.wikipedia.org/wiki/Provincia_autonoma_di_Bolzano](https://it.wikipedia.org/wiki/Provincia_autonoma_di_Bolzano)