import pandas as pd
import datetime
import dash
import pickle
import dash_core_components as dcc
import dash_html_components as html
import numpy as np
from dash.dependencies import Input, Output
from data_getter import data
from threading import Thread

################ THREAD STUFF ###################

class DataThread(Thread):

	def __init__(self, regions, update=False):
		Thread.__init__(self)
		self.regions = regions
		self.to_ret = {}
		self.update = update

	def run(self):
		if self.update == True:
			for i in self.regions:
				print(i)
				if i == "Totale":
					curr = data.get_nazione()
				else:
					curr = data.get_regioni(name=i)

				curr["data"] = curr["data"].str.slice(0, -9)
				curr["data"] = pd.to_datetime(curr["data"])

				self.to_ret[i] = curr

			with open('all_data', 'wb') as fdata:
				pickle.dump(self.to_ret, fdata)
		
		return

	def join(self):
		with open('all_data', 'rb') as fdata:
			foo = pickle.load(fdata)
		
		return foo

################ APPLICATION STUFF ##############

app = dash.Dash(serve_locally=False)

app.layout = html.Div([
    html.Div([
        html.H4('COVID-19 Italy',
                style={'float': 'left',
                       }),
        ]),
    dcc.Input(id='input', value='Italia, Piemonte, Lombardia', type='text', className='autocomplete'),
    html.Div(children=html.Div(id='graphs'), className='row'),
    ], className="container",style={'width':'90%','margin-left':50,'margin-right':50})


@app.callback(
    Output(component_id='graphs', component_property='children'),
    [Input(component_id='input', component_property='value')]
)

def update_value(input_data):

	input_data = input_data.split(", ")

	print(len(all_data))

	tot = []
	new = []
	perc = []
	incr = []
	for i in input_data:
		try:		
			i = str(i)
			i = i[0].upper() + i[1:].lower() 

			if i == "Italy" or i == "Italia":
				if len(all_data) == 20:
					toplt = all_data["Totale"]
				else:
					toplt = data.get_nazione()
			
			else:
				if len(all_data) == 20:
					toplt = all_data[i]
				else:
					toplt = data.get_regioni(name=str(i))
			
			tot.append({'x': toplt.data, 'y': toplt.totale_positivi.dropna(), 'type': 'line', 'name': i})
			new.append({'x': toplt.data, 'y': toplt.nuovi_positivi.dropna(), 'type': 'line', 'name': i})
			perc.append({'x': toplt.data, 'y': (toplt.totale_positivi/toplt.casi_testati).dropna(), 'type': 'bar', 'name': i})

		except Exception as e:
			print("Error:")
			print(e)
			return "No region with that name"
	
	foo = [0]
	for i in range(1,len(all_data["Totale"].dimessi_guariti)):
		foo.append(all_data["Totale"].dimessi_guariti[i] - all_data["Totale"].dimessi_guariti[i-1])

	boo = [0]
	for i in range(1,len(all_data["Totale"].deceduti)):
		boo.append(all_data["Totale"].deceduti[i] - all_data["Totale"].deceduti[i-1])			
	
	diff = all_data["Totale"].nuovi_positivi - (np.array(np.array(foo) + np.array(boo)))
	incr.append({'x': all_data["Totale"].data, 'y': diff.dropna().rolling(4).mean(), 'type': 'line', 'name': i})

	to_ret = [

	dcc.Graph(
		id='tot_pos',
		figure={
	    	'data': tot,
	    	'layout': {'title': 'Totale positivi'}
		}
	),
	dcc.Graph(
		id='new_pos',
		figure={
			'data': new,
			'layout': {'title': 'Nuovi positivi'}
		}
	),
	dcc.Graph(
		id='perc',
		figure={
			'data': perc,
			'layout': {'title': 'Casi / casi testati'}
		},
	),
	dcc.Graph(
		id='incr',
		figure={
			'data': incr,
			'layout': {'title': 'Nuovi positivi - (dimessi+deceduti) -- ITALIA'}
		},
	)
	]

	return to_ret

external_css = ["https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css"]
for css in external_css:
	app.css.append_css({"external_url": css})

if __name__ == '__main__':

	regs = regs = pd.read_html('http://www.comuni-italiani.it/regioni.html')[3][0].drop(0).to_list()
	regs[regs.index("Trentino-Alto Adige")] = "P.A. Trento"
	regs.remove("Valle d'Aosta")
	thr = DataThread(regs, update=False)
	thr.start()
	all_data = thr.join()
	# with open('all_data', 'rb') as fdata:
	# 	all_data = pickle.load(fdata)
	app.run_server(host= '0.0.0.0', debug=True)