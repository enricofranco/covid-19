import datetime
import dash
import dash_bootstrap_components as dbc
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output
from data_getter import data
import plotly.graph_objects as go
from plotly.subplots import make_subplots
import numpy as np
import pandas as pd
import time

italia = data.get_nazione()
regioni = data.get_regioni()

def get_nomi_regioni():

    to_ret = []
    nomi_regioni = regioni.denominazione_regione.unique()
    to_ret.append({
        'label': 'Italia',
        'value': 'Italia'
    })

    for r in nomi_regioni:
        to_ret.append({
        'label': r,
        'value': r
    })

    return to_ret

def get_data():

    to_ret = {}
    
    italia.data = italia.data.str.slice(0, -9)
    italia.data = pd.to_datetime(italia.data)
    to_ret['Italia'] = italia

    nomi_regioni = regioni.denominazione_regione.unique()
    col_name = 'denominazione_regione'
    for r in nomi_regioni:
        query = '{} == "{}"'.format(col_name, r)
        regione = regioni.query(query)
        regione.data = regione.data.str.slice(0, -9)
        regione.data = pd.to_datetime(regione.data)
        to_ret[r] = regione

    return to_ret

app = dash.Dash(
    serve_locally = False,
    external_stylesheets = [dbc.themes.BOOTSTRAP],
)

app.title = 'COVID19 - Plots'
app.layout = dbc.Container(
    fluid = True,
    children = [
        # Title row
        dbc.Row(dbc.Col(html.Div([
            html.H1('COVID19 - Plots'),
            html.Hr(),
        ]))),
        # Selection row
        dbc.Row(dbc.Col(html.Div(
            html.Form([
                dbc.FormGroup([
                    dcc.Dropdown(
                        id = 'regions',
                        persistence = False,
                        options = get_nomi_regioni(),
                        value = ['Italia', 'Piemonte', 'Lombardia', 'Emilia-Romagna'],
                        multi = True,
                    ),
                    dbc.FormText('Select regions to plot', color = 'secondary')
                ]),
                dbc.FormGroup([
                    dbc.Input(
                        id = 'start-date',
                        className = 'form-control',
                        type = 'date',
                        value = '2020-03-01'
                    ),
                    dbc.FormText('Select start date', color = 'secondary'),
                ]),
                dbc.FormGroup([
                    dbc.Input(
                        id = 'end-date',
                        className = 'form-control',
                        type = 'date',
                        value = datetime.date.today().strftime('%Y-%m-%d')
                    ),
                    dbc.FormText('Select end date', color = 'secondary'),
                ])
            ]),
        ))),
        # Plots row
        dbc.Row(dbc.Col(html.Div(id = 'output-graph')))
    ]
)

@app.callback(
    Output(component_id = 'output-graph', component_property = 'children'),
    [
        Input(component_id = 'regions', component_property = 'value'),
        Input(component_id = 'start-date', component_property = 'value'),
        Input(component_id = 'end-date', component_property = 'value')
    ]
)
def update_plots(regions, start_date, end_date):
    
    tot = []
    rate_totale_positivi = []
    new = []
    perc = []

    # Add mixed plot
    fig = make_subplots(specs=[[{"secondary_y": True}]])
    fig.update_layout(
        title = 'Positive cases vs Rate',
    )
    fig.update_yaxes(tickformat = '%', secondary_y = True)

    for i in regions:
        try:
            # Filter by input date
            query = "'{}' <= data <= '{}'".format(start_date, end_date)
            toplt = all_data[i].query(query)

            # Calculate rate wrt previous day
            rtp = np.diff(toplt.totale_positivi) / toplt.totale_positivi[:-1]

            tot.append({
                'x': toplt.data,
                'y': toplt.totale_positivi,
                'type': 'line',
                'name': i,
                'mode': 'lines+markers',
            })
            rate_totale_positivi.append({
                'x': toplt.data[1:],
                'y': rtp,
                'type': 'line',
                'name': i,
                'mode': 'lines+markers',
                'hovertemplate': '%{y:+.2%}' # Layout type: +1.23%
            })
            new.append({
                'x': toplt.data,
                'y': toplt.nuovi_positivi,
                'type': 'line',
                'name': i,
                'mode': 'lines+markers',
            })
            perc.append({
                'x': toplt.data,
                'y': toplt.totale_positivi / toplt.casi_testati,
                'type': 'bar',
                'name': i,
                'hovertemplate': '%{y:+.2%}' # Layout type: +1.23%
            })

            # Add to mixed plot
            fig.add_trace(
                go.Scatter(
                    x = toplt.data,
                    y = toplt.totale_positivi,
                    mode = 'lines+markers',
                    fill = 'tozeroy',
                    name = '[{}] Positive cases'.format(i)
                ),
                secondary_y = False
            )
            fig.add_trace(
                go.Scatter(
                    x = toplt.data[1:],
                    y = np.diff(toplt.totale_positivi) / toplt.totale_positivi[:-1],
                    name = '[{}] Positive cases rate'.format(i),
                    mode = 'lines+markers',
                    hovertemplate = '%{y:+.2%}' # Layout type: +1.23%
                ),
                secondary_y = True
            )

        except Exception as e:
            print(e)
            return "No region with that name"

    to_ret = [
        dcc.Graph(
            id = 'tot_pos',
            figure = {
                'data': tot,
                'layout': {'title': 'Totale positivi'}
            }
        ),
        dcc.Graph(
            id = 'rate_tot_pos',
            figure = {
                'data': rate_totale_positivi,
                'layout': {
                    'title': 'Tasso totale positivi',
                    'yaxis': {'tickformat': '%'}
                }
            }
        ),
        dcc.Graph(
            id = 'new_pos',
            figure = {
                'data': new,
                'layout': {'title': 'Nuovi positivi'}
            }
        ),
        dcc.Graph(
            id = 'perc',
            figure = {
                'data': perc,
                'layout': {
                    'title': 'Rapporto Positivi vs Casi testati',
                    'yaxis': {'tickformat': '%'}
                }
            }
        ),
        dcc.Graph(figure = fig)
	]

    return to_ret

if __name__ == '__main__':
    all_data = get_data()
    app.run_server(host = '0.0.0.0', debug = True)