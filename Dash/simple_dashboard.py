import dash
import dash_core_components as dcc
import dash_html_components as html
import pandas as pd
from data_getter import data

naz = data.get_nazione()
naz["data"] = naz["data"].str.slice(0, -9)
naz["data"] = pd.to_datetime(naz["data"])

lom = data.get_regioni(name="Lombardia")
lom["data"] = lom["data"].str.slice(0, -9)
lom["data"] = pd.to_datetime(lom["data"])

piem = data.get_regioni(name="Piemonte")
piem["data"] = piem["data"].str.slice(0, -9)
piem["data"] = pd.to_datetime(piem["data"])


app = dash.Dash()

app.layout = html.Div(children=[
    html.H1(children='COVID-19 - Italy'),
    dcc.Graph(
        id='tot_pos',
        figure={
            'data': [
                {'x': naz.data, 'y': naz.totale_positivi, 'type': 'line', 'name': 'Nazione'},
                {'x': lom.data, 'y': lom.totale_positivi, 'type': 'line', 'name': 'Lombardia'},
                {'x': piem.data, 'y': piem.totale_positivi, 'type': 'line', 'name': 'Piemonte'},
            ],
            'layout': {
                'title': 'Totale positivi'
            }
        }
    ),
    dcc.Graph(
        id='new_pos',
        figure={
            'data': [
                {'x': naz.data, 'y': naz.nuovi_positivi, 'type': 'line', 'name': 'Nazione'},
                {'x': lom.data, 'y': lom.nuovi_positivi, 'type': 'line', 'name': 'Lombardia'},
                {'x': piem.data, 'y': piem.nuovi_positivi, 'type': 'line', 'name': 'Piemonte'},
            ],
            'layout': {
                'title': 'Nuovi positivi'
            }
        }
    ),
    dcc.Graph(
        id='perc',
        figure={
            'data': [
                {'x': naz.data, 'y': naz.totale_positivi/naz.casi_testati, 'type': 'line', 'name': 'Nazione'},
                {'x': lom.data, 'y': lom.totale_positivi/lom.casi_testati, 'type': 'line', 'name': 'Lombardia'},
                {'x': piem.data, 'y': piem.totale_positivi/piem.casi_testati, 'type': 'line', 'name': 'Piemonte'},
            ],
            'layout': {
                'title': 'Casi / casi testati'
            }
        }
    )
])

if __name__ == '__main__':
    app.run_server(host= '0.0.0.0',debug=True)