import sys
import matplotlib
import numpy as np
import matplotlib as plt
import matplotlib.pyplot as plt
import matplotlib.animation as anim

# -- POPULATION MATRIX -- #
# 0 : unique ID
# 1 : current x coordinate
# 2 : current y coordinate
# 3 : current heading in x direction
# 4 : current heading in y direction
# 5 : current speed
# 6 : current state --> 0=healthy, 1=positive, 2=sick, 3=immune (dead or recovered)
# 7 : age
# 8 : infected_since (frame the person got infected)
# 9 : quarantined

# Enumerator for default values for covid-19
class covid:

	INFECTION_PROB = 0.2
	INFECTION_RANGE = 0.01
	DEATH_PROB = [0.01, 0.05, 0.15]
	RECOVERY_TIME = 70
	SYMP_DELAY = 5

	# Prob to infect someone if quarantined
	QUARANTINE_INFECTION_PROB = 0.02


def population_init(size=1000, age_range=[20,70]):

	# Empty matrix
	population = np.zeros((size, 10))

	# init id
	population[:,0] = [x for x in range(size)]

	# initial positions x,y
	population[:,1] = np.random.uniform(low=0.05, high=0.95, size=size)
	population[:,2] = np.random.uniform(low = 0.05, high = 0.95, size=size)

	# init direction (-1,1)
	population[:,3] = np.random.normal(loc=0, scale=1/3, size=size)
	population[:,4] = np.random.normal(loc=0, scale=1/3, size=size)

	#initialize random speeds
	population[:,5] = np.random.normal(0.01, 0.01/3)

	#initalize ages
	population[:,7] = np.int32(np.random.uniform(low=age_range[0], high=age_range[1], size=size))

	#build recovery_vector
	population[:,9] = np.random.normal(loc = 0.5, scale = 0.5 / 3, size=size)

	return population

def move(population, bounds=0.05):

	min_coor = 0.05
	max_coor = 0.95

	
	# moves x in the current direction and speed
	population[:,1] = population[:,1] + (population[:,3] * population[:,5])
	
	# moves y in the current direction and speed
	population[:,2] = population[:,2] + (population [:,4] * population[:,5])


	for i in range(len(population[:,1])):

		if(population[i,1] < 0.05 or population[i,1] > 0.95):
			population[i,3] = np.random.random()
			population[i,1] = np.random.random()
		
		if(population[i,2] < 0.05 or population[i,2] > 0.95):
			population[i,4] = np.random.random()
			population[i,2] = np.random.random()

	return population

def get_contact(coord_a, coord_b):

	return True 

def infection(population):


	# Adds days to infection
	pos = np.int32(population[population[:,6]==1][:,0])
	population[pos,8] += 1

	old = population[population[:,6]==1]
	new = 0

	for i in old:

		# get current positive
		positive_population = population[population[:,6]==1]
		negative_population = population[population[:,6]==0]

		# Get infected area
		area = [i[1] - covid.INFECTION_RANGE, i[2] - covid.INFECTION_RANGE,
				i[1] + covid.INFECTION_RANGE, i[2] + covid.INFECTION_RANGE]

		# Find new infected
		new_positive_ids = np.int32(negative_population[:,0][(area[0] < negative_population[:,1]) & 
                                               				 (negative_population[:,2] < area[2]) &
                                               				 (area[1] < negative_population [:,1]) & 
                                               				 (negative_population[:,2] < area[3])])
		
		new += len(new_positive_ids)
		
		# Update new positives
		for idx in new_positive_ids:    
			
			if np.random.random() < covid.INFECTION_PROB:
				
				population[idx][6] = 1
				population[idx][8] = 1

		# If symp appeared --> mark as quarantined
		if(i[8] > covid.SYMP_DELAY):
			population[int(i[0])][9] = 1

		# If recovered --> mark as recovered or dead
		if(i[8] > covid.RECOVERY_TIME):
			print("here")
			population[int(i[0])][6] = 3

	
	print(new)

	return population

def update(frame, population, infection_range=0.01, infection_chance=0.03, 
           recovery_duration=(200, 500), mortality_chance=0.02,
           xbounds=[0.02, 0.98], ybounds=[0.02, 0.98], wander_range=0.05,
           visualise=True, infected_plot = []):

    #add one infection to jumpstart
    if frame == 20:
        population[0][6] = 1
        population[0][8] = 75

    #update out of bounds
    #define bounds arrays
    _xbounds = np.array([[xbounds[0] + 0.02, xbounds[1] - 0.02]] * len(population))
    _ybounds = np.array([[ybounds[0] + 0.02, ybounds[1] - 0.02]] * len(population))

	#for dead ones: set speed and heading to 0
    population[:,3:5][population[:,6] == 3] = 0

    #update positions
    population = move(population)
    
    #find new infections
    population = infection(population)
    infected_plot.append(len(population[population[:,6] == 1]))



    if visualise:
        #construct plot and visualise
        spec = fig.add_gridspec(ncols=1, nrows=2, height_ratios=[5,2])
        ax1.clear()
        ax2.clear()

        ax1.set_xlim(xbounds[0] - 0.02, xbounds[1] + 0.02)
        ax1.set_ylim(ybounds[0] - 0.02, ybounds[1] + 0.02)

        healthy = population[population[:,6] == 0][:,1:3]
        ax1.scatter(healthy[:,0], healthy[:,1], color='gray', s = 2, label='healthy')
    
        infected = population[population[:,6] == 1][:,1:3]
        ax1.scatter(infected[:,0], infected[:,1], color='red', s = 2, label='infected')

        immune = population[population[:,6] == 2][:,1:3]
        ax1.scatter(immune[:,0], immune[:,1], color='green', s = 2, label='sick')
    
        fatalities = population[population[:,6] == 3][:,1:3]
        ax1.scatter(fatalities[:,0], fatalities[:,1], color='black', s = 2, label='immune or dead')
    
        #add text descriptors
        ax1.text(xbounds[0], 
                 ybounds[1] + 0.03, 
                 'timestep: %i, total: %i, healthy: %i infected: %i sick: %i rec or dead: %i' %(frame,
                                                                                              len(population),
                                                                                              len(healthy), 
                                                                                              len(infected), 
                                                                                              len(immune), 
                                                                                              len(fatalities)),
                 fontsize=6)
    
        ax2.set_title('number of infected')

        ax2.set_ylim(0, pop_size + 100)
        ax2.plot(infected_plot, color='gray')
        plt.savefig('render/a%s.png' %frame)

if __name__ == '__main__':

	#set simulation parameters
	pop_size = 1000
	simulation_steps = 20000
	xbounds = [0, 1] 
	ybounds = [0, 1]

	population = population_init(pop_size)

	#define figure
	fig = plt.figure(figsize=(5,7))
	spec = fig.add_gridspec(ncols=1, nrows=2, height_ratios=[5,2])

	ax1 = fig.add_subplot(spec[0,0])
	plt.title('infection simulation')
	plt.xlim(xbounds[0], xbounds[1])
	plt.ylim(ybounds[0], ybounds[1])

	ax2 = fig.add_subplot(spec[1,0])
	ax2.set_title('number of infected')
	ax2.set_xlim(0, simulation_steps)
	ax2.set_ylim(0, pop_size + 100)

	#start animation loop through matplotlib visualisation
	animation = anim.FuncAnimation(fig, update, fargs = (population,), frames = simulation_steps, interval = 50)
	plt.show()