import matplotlib.pyplot as plt
import numpy as np
from data_getter import data

def seir_model(init_vals, params, t):
    
    S_0, E_0, I_0, R_0 = init_vals
    S, E, I, R = [S_0], [E_0], [I_0], [R_0]
    
    alpha_distr, beta, gamma_distr, rho = params
    
    dt = t[1] - t[0]
    cont = 0

    for _ in t[1:]:
        
        cont += 1

        # Extracts value from distributions
        alpha = 1./np.random.choice(alpha_distr, 1)
        gamma = 1./np.random.choice(gamma_distr, 1)

        if(cont < 80):
            # Calculate values in a normal period
            next_S = S[-1] - (beta*S[-1]*I[-1])*dt
            next_E = E[-1] + (beta*S[-1]*I[-1] - alpha*E[-1])*dt
            next_I = I[-1] + (alpha*E[-1] - gamma*I[-1])*dt
            next_R = R[-1] + (gamma*I[-1])*dt
        else:
            # Calculate values in a quarantene period    
            next_S = S[-1] - (rho*beta*S[-1]*I[-1])*dt
            next_E = E[-1] + (rho*beta*S[-1]*I[-1] - alpha*E[-1])*dt
            next_I = I[-1] + (alpha*E[-1] - gamma*I[-1])*dt
            next_R = R[-1] + (gamma*I[-1])*dt
        
        # Adds new values
        S.append(next_S)
        E.append(next_E)
        I.append(next_I)
        R.append(next_R)
    
    return (S, E, I, R)


# Initial values
s0 = 1
e0 = 0.01
i0 = 0
r0 = 0

# --------- parameters --------- #
alpha = np.random.normal(7, 0.5, 50)                # incubation period
gamma = np.random.normal(10, 0.5, 50)               # 1/infectuos_period
rho = 0.5                                           # quarantine
beta = 0.35                                         # contact rate

init = (s0, e0, i0, r0)
params = (alpha, beta, gamma, rho)
time = np.linspace(0,150,150)

s,e,i,r = seir_model(init, params, time)

plt.plot(time, s)
plt.plot(time, e)
plt.plot(time, i)
plt.plot(time, r)

naz = data.get_nazione()

plt.plot(naz["totale_positivi"]/600000)

plt.title('SEIR model')
plt.legend(['Susceptible', 'Exposed', 'Infected', 'Recovered', 'Italy'])

plt.show()