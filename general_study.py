from data_getter import data
import matplotlib.pyplot as plt
from sklearn import linear_model
from sklearn.metrics import mean_squared_error
from sklearn.metrics import mean_absolute_error
import numpy as np
import pandas

DATE = '2020-03-23'

def get_tasso(y):

    y = np.array(y)
    per_day = [0]
    for i in range(1, len(y)):
        if y[i-1] == 0:
            per_day.append(0)
        else:
            per_day.append((y[i] - y[i-1]) / y[i-1] * 100)
    return per_day

def get_increment(y):

    y = np.array(y)
    increment = [y[0]]
    for i in range(1, len(y)):
        increment.append(y[i] - y[i-1])
    return increment

def general_plot(df, title):

    # X axis
    date = [i[5:-9] for i in df.data]

    # Rates
    tasso_deceduti = get_tasso(df.deceduti)
    tasso_positivi = get_tasso(df.totale_positivi)
    tasso_ospedalizzati = get_tasso(df.totale_ospedalizzati)
    tasso_terapia_intensiva = get_tasso(df.terapia_intensiva)
    tasso_dimessi_guariti = get_tasso(df.dimessi_guariti)

    # General numbers
    plt.figure()
    plt.plot(date, df.totale_positivi, 'o-', label = 'Contagiati {} ({:+.2f}%)'.format(df.totale_positivi.iloc[-1], tasso_positivi[-1]))
    plt.plot(date, df.dimessi_guariti, 'o-', label = 'Guariti: {} ({:+.2f}%)'.format(df.dimessi_guariti.iloc[-1], tasso_dimessi_guariti[-1]))
    plt.plot(date, df.totale_ospedalizzati, 'o-', label = 'Ospedalizzati: {} ({:+.2f}%)'.format(df.totale_ospedalizzati.iloc[-1], tasso_ospedalizzati[-1]))
    plt.plot(date, df.terapia_intensiva, 'o-', label = 'Terapia intensiva: {} ({:+.2f}%)'.format(df.terapia_intensiva.iloc[-1], tasso_terapia_intensiva[-1]))
    plt.plot(date, df.deceduti, 'o-', label = 'Deceduti: {} ({:+.2f}%)'.format(df.deceduti.iloc[-1], tasso_deceduti[-1]))
    plt.legend()
    plt.grid(True)
    plt.title(title)

    for x, y in zip(date, df.totale_positivi):
        label = y
        plt.annotate(label, # text
                    (x, y), # point to label
                    textcoords = 'offset points', # position
                    xytext = (0, 7), # distance
                    ha = 'center') # horizontal alignment

    for x, y, t in zip(date[1:], df.totale_positivi[1:], tasso_positivi[1:]):
        label = '{:+.2f}%'.format(t)
        plt.annotate(label, # text
                    (x, y), # point to label
                    textcoords = 'offset points', # position
                    xytext = (0, -15), # distance
                    ha = 'center') # horizontal alignment

def plot_totale_casi(nazione, lombardia, piemonte):

    # X axis
    date = [i[5:-9] for i in nazione.data]

    tasso_nazione = get_tasso(nazione.totale_casi)
    tasso_lombardia = get_tasso(lombardia.totale_casi)
    tasso_piemonte = get_tasso(piemonte.totale_casi)

    # General numbers
    plt.figure()
    plt.plot(date, nazione.totale_casi, 'o-', label = 'Italia: {} ({:+.2f}%)'.format(nazione.totale_casi.iloc[-1], tasso_nazione[-1]))
    plt.plot(date, lombardia.totale_casi, 'o-', label = 'Lombardia: {} ({:+.2f}%)'.format(lombardia.totale_casi.iloc[-1], tasso_lombardia[-1]))
    plt.plot(date, piemonte.totale_casi, 'o-', label = 'Piemonte: {} ({:+.2f}%)'.format(piemonte.totale_casi.iloc[-1], tasso_piemonte[-1]))
    plt.legend()
    plt.grid(True)
    plt.title('Totale casi')

    # Internal function
    def plot_labels(df, tasso):

        for x, y in zip(date, df.totale_casi):
            label = y
            plt.annotate(label, # text
                        (x, y), # point to label
                        textcoords = 'offset points', # position
                        xytext = (0, 7), # distance
                        ha = 'center') # horizontal alignment

        for x, y, t in zip(date[-5:], df.totale_casi[-5:], tasso[-5:]):
            label = '{:+.2f}%'.format(t)
            plt.annotate(label, # text
                        (x, y), # point to label
                        textcoords = 'offset points', # position
                        xytext = (0, -15), # distance
                        ha = 'center') # horizontal alignment    

    # Add labels
    plot_labels(nazione, tasso_nazione)
    plot_labels(lombardia, tasso_lombardia)
    plot_labels(piemonte, tasso_piemonte)

    date_day0 = '2020-03-22'
    query_day0 = "data.str.contains('{}')".format(date_day0)
    totale_casi_nazione_day0 = data.get_nazione().query(query_day0).totale_casi.iloc[0].item()
    totale_casi_piemonte_day0 = data.get_regioni(name = 'Piemonte').query(query_day0).totale_casi.iloc[0].item()
    totale_casi_lombardia_day0 = data.get_regioni(name = 'Lombardia').query(query_day0).totale_casi.iloc[0].item()

    tn0 = (nazione.totale_casi.iloc[0].item() - totale_casi_nazione_day0) / totale_casi_nazione_day0 * 100.0
    tp0 = (piemonte.totale_casi.iloc[0].item() - totale_casi_piemonte_day0) / totale_casi_piemonte_day0 * 100.0
    tl0 = (lombardia.totale_casi.iloc[0].item() - totale_casi_lombardia_day0) / totale_casi_lombardia_day0 * 100.0

    # General numbers
    plt.figure()
    plt.plot(date, [tn0] + tasso_nazione[1:], 'o-', label = 'Italia ({:+.2f}%)'.format(tasso_nazione[-1]))
    plt.plot(date, [tl0] + tasso_lombardia[1:], 'o-', label = 'Lombardia ({:+.2f}%)'.format(tasso_lombardia[-1]))
    plt.plot(date, [tp0] + tasso_piemonte[1:], 'o-', label = 'Piemonte ({:+.2f}%)'.format(tasso_piemonte[-1]))
    plt.legend()
    plt.grid(True)
    plt.title('Tasso totale casi')

def plot_tamponi(nazione, lombardia, piemonte):
    
    # X axis
    date = [i[5:-9] for i in nazione.data]

    # Internal function
    def get_swab_total_ratio(df):
        casi = np.array(df.totale_casi)
        tamponi = np.array(df.tamponi)
        ratio = casi / tamponi * 100.0
        return ratio

    # Internal function
    def get_swab_positive_ratio(df):
        positivi = np.array(df.totale_positivi)
        tamponi = np.array(df.tamponi)
        ratio = positivi / tamponi * 100.0
        return ratio

    # Internal function
    def plot_labels(to_plot, tasso, limit = 5):

        for x, y, t in zip(date[-limit:], to_plot[-limit:], tasso[-limit:]):
            label = '{:+.2f}%'.format(t)
            plt.annotate(label, # text
                        (x, y), # point to label
                        textcoords = 'offset points', # position
                        xytext = (0, 7), # distance
                        ha = 'center') # horizontal alignment    

    ### Tamponi
    plt.figure()
    top = plt.subplot(2, 1, 1)
    top.set_title('Rapporto tamponi-totale casi')
    ratio_nazione = get_swab_total_ratio(nazione)
    ratio_lombardia = get_swab_total_ratio(lombardia)
    ratio_piemonte = get_swab_total_ratio(piemonte)
    rate_nazione = get_tasso(ratio_nazione)
    rate_lombardia = get_tasso(ratio_lombardia)
    rate_piemonte= get_tasso(ratio_piemonte)
    plt.plot(date, ratio_nazione, 'o-', label = 'Italia')
    plt.plot(ratio_lombardia, 'o-', label = 'Lombardia')
    plt.plot(ratio_piemonte, 'o-', label = 'Piemonte')
    plot_labels(ratio_nazione, rate_nazione)
    plot_labels(ratio_lombardia, rate_lombardia)
    plot_labels(ratio_piemonte, rate_piemonte)
    plt.grid(True)
    plt.legend()
    
    bottom = plt.subplot(2, 1, 2)
    bottom.set_title('Rapporto tamponi-positivi')
    ratio_nazione = get_swab_positive_ratio(nazione)
    ratio_lombardia = get_swab_positive_ratio(lombardia)
    ratio_piemonte = get_swab_positive_ratio(piemonte)
    rate_nazione = get_tasso(ratio_nazione)
    rate_lombardia = get_tasso(ratio_lombardia)
    rate_piemonte= get_tasso(ratio_piemonte)
    plt.plot(date, ratio_nazione, 'o-', label = 'Italia')
    plt.plot(ratio_lombardia, 'o-', label = 'Lombardia')
    plt.plot(ratio_piemonte, 'o-', label = 'Piemonte')
    plot_labels(ratio_nazione, rate_nazione)
    plot_labels(ratio_lombardia, rate_lombardia)
    plot_labels(ratio_piemonte, rate_piemonte)
    plt.grid(True)
    plt.legend()

    def get_rates(a, b, c):

        a = np.array(a)
        b = np.array(b)
        c = np.array(c)
        return get_tasso(a), get_tasso(b), get_tasso(c)

    plt.figure()
    top = plt.subplot(2, 1, 1)
    top.set_title('Nuovi tamponi')
    plt.plot(date, np.array(nazione.nuovi_tamponi), 'o-', label = 'Italia')
    plt.plot(np.array(lombardia.nuovi_tamponi), 'o-', label = 'Lombardia')
    plt.plot(np.array(piemonte.nuovi_tamponi), 'o-', label = 'Piemonte')
    plt.grid(True)
    plt.legend()

    rate_nazione, rate_lombardia, rate_piemonte = get_rates(nazione.nuovi_tamponi, lombardia.nuovi_tamponi, piemonte.nuovi_tamponi)
    plot_labels(np.array(nazione.nuovi_tamponi), rate_nazione, limit = 3)
    plot_labels(np.array(lombardia.nuovi_tamponi), rate_lombardia, limit = 3)
    plot_labels(np.array(piemonte.nuovi_tamponi), rate_piemonte, limit = 3)

    bottom = plt.subplot(2, 1, 2)
    bottom.set_title('Rapporto nuovi positivi-nuovi tamponi')
    plt.plot(date, np.array(nazione.nuovi_positivi) / np.array(nazione.nuovi_tamponi) * 100.0, 'o-', label = 'Italia')
    plt.plot(np.array(lombardia.nuovi_positivi) / np.array(lombardia.nuovi_tamponi) * 100.0, 'o-', label = 'Lombardia')
    plt.plot(np.array(piemonte.nuovi_positivi) / np.array(piemonte.nuovi_tamponi) * 100.0, 'o-', label = 'Piemonte')
    plt.grid(True)
    plt.legend()

    rate_nazione, rate_lombardia, rate_piemonte = get_rates(np.array(nazione.nuovi_positivi) / np.array(nazione.nuovi_tamponi) * 100.0,
        np.array(lombardia.nuovi_positivi) / np.array(lombardia.nuovi_tamponi) * 100.0,
        np.array(piemonte.nuovi_positivi) / np.array(piemonte.nuovi_tamponi) * 100.0)
    plot_labels(np.array(nazione.nuovi_positivi) / np.array(nazione.nuovi_tamponi) * 100.0, rate_nazione, limit = 3)
    plot_labels(np.array(lombardia.nuovi_positivi) / np.array(lombardia.nuovi_tamponi) * 100.0, rate_lombardia, limit = 3)
    plot_labels(np.array(piemonte.nuovi_positivi) / np.array(piemonte.nuovi_tamponi) * 100.0, rate_piemonte, limit = 3)

def clean_pandas_dataframe(df):

    df.drop(['stato', 'ricoverati_con_sintomi', 'isolamento_domiciliare', 'note_it', 'note_en'], axis = 'columns', inplace = True)
    df['nuovi_tamponi'] = get_increment(df.tamponi)
    df['nuovi_dimessi_guariti'] = get_increment(df.dimessi_guariti)
    df['nuovi_deceduti'] = get_increment(df.deceduti)
    df['differenza_positivi_guariti'] = df.nuovi_positivi - df.nuovi_dimessi_guariti
    df['differenza_positivi_deceduti'] = df.nuovi_positivi - df.nuovi_deceduti
    df['differenza_positivi_guariti_deceduti'] = df.nuovi_positivi - df.nuovi_dimessi_guariti - df.nuovi_deceduti
    return df.query("data >= '{}'".format(DATE))

def plot_differenze(nazione, lombardia, piemonte):

    # X axis
    date = [i[5:-9] for i in nazione.data]

    tasso_differenza_positivi_guariti = get_tasso(nazione.differenza_positivi_guariti)
    tasso_differenza_positivi_deceduti = get_tasso(nazione.differenza_positivi_deceduti)
    tasso_differenza_positivi_guariti_deceduti = get_tasso(nazione.differenza_positivi_guariti_deceduti)

    # General numbers
    plt.figure()
    plt.plot(date, nazione.differenza_positivi_guariti, 'o-', label = 'Differenza positivi - guariti: {} ({:+.2f}%)'.format(nazione.differenza_positivi_guariti.iloc[-1], tasso_differenza_positivi_guariti[-1]))
    plt.plot(date, nazione.differenza_positivi_deceduti, 'o-', label = 'Differenza positivi - deceduti: {} ({:+.2f}%)'.format(nazione.differenza_positivi_deceduti.iloc[-1], tasso_differenza_positivi_deceduti[-1]))
    plt.plot(date, nazione.differenza_positivi_guariti_deceduti, 'o-', label = 'Differenza positivi - guariti - deceduti: {} ({:+.2f}%)'.format(nazione.differenza_positivi_guariti_deceduti.iloc[-1], tasso_differenza_positivi_guariti_deceduti[-1]))
    plt.legend()
    plt.grid(True)
    plt.title('Italia')

    # Internal function
    def plot_labels(series, tasso):

        for x, y in zip(date[-3:], series[-3:]):
            label = y
            plt.annotate(label, # text
                        (x, y), # point to label
                        textcoords = 'offset points', # position
                        xytext = (0, 7), # distance
                        ha = 'center') # horizontal alignment

        for x, y, t in zip(date[-5:], series[-5:], tasso[-5:]):
            label = '{:+.2f}%'.format(t)
            plt.annotate(label, # text
                        (x, y), # point to label
                        textcoords = 'offset points', # position
                        xytext = (0, -15), # distance
                        ha = 'center') # horizontal alignment    

    # Add labels
    plot_labels(nazione.differenza_positivi_guariti, tasso_differenza_positivi_guariti)
    plot_labels(nazione.differenza_positivi_deceduti, tasso_differenza_positivi_deceduti)
    plot_labels(nazione.differenza_positivi_guariti_deceduti, tasso_differenza_positivi_guariti_deceduti)

if __name__ == '__main__':

    # Get all data
    nazione = data.get_nazione()
    lombardia = data.get_regioni(name = 'Lombardia')
    piemonte = data.get_regioni(name = 'Piemonte')

    # Add stub increment
    lombardia['nuovi_tamponi'] = get_increment(lombardia.tamponi)
    piemonte['nuovi_tamponi'] = get_increment(piemonte.tamponi)

    # Clean, elaborate and filter
    nazione = clean_pandas_dataframe(nazione)
    lombardia = clean_pandas_dataframe(lombardia)
    piemonte = clean_pandas_dataframe(piemonte)

    general_plot(nazione, 'Italia')
    general_plot(piemonte, 'Piemonte')
    general_plot(lombardia, 'Lombardia')

    plot_totale_casi(nazione, lombardia, piemonte)
    plot_tamponi(nazione, lombardia, piemonte)

    plot_differenze(nazione, lombardia, piemonte)

    plt.show()
