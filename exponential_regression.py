from data_getter import data
import matplotlib.pyplot as plt
from sklearn import linear_model
from sklearn.metrics import mean_squared_error
from sklearn.metrics import mean_absolute_error
import numpy as np
import pandas

POPOLAZIONE = './data/popolazione.csv'
LIMITAZIONI = '03-08'

def get_attualmente_positivi(df):

    x = range(1, len(df.data)+1)
    y = df.totale_positivi

    return x, y

def prepare_log_model(x, y, end = 0):

    x = x[:len(x) - end]
    y = y[:len(y) - end]
    biased = np.polyfit(x, np.log(y), 1)
    unbiased = np.polyfit(x, np.log(y), 1, w=np.sqrt(y))

    return biased, unbiased

def prepare_linear_model(x, y, end = 0):

    x = x[:len(x) - end]
    y = y[:len(y) - end]
    biased = np.polyfit(x, y, 1)
    unbiased = np.polyfit(x, y, 1, w=np.sqrt(y))

    return biased, unbiased

def get_tasso_contagio(df):

    per_day = [0]
    y = np.array(df.totale_positivi)
    for i in range(1,len(y)):
        if y[i-1] == 0:
            # per_day.append(per_day[i-2])
            per_day.append(0)
        else:
            per_day.append((y[i] - y[i-1]) / y[i-1] * 100)
    
    return per_day

def get_tasso_contagio_ponderato_tamponi(df):

    per_day = [0]
    y = np.array(df.totale_positivi / df.tamponi)
    for i in range(1,len(y)):
        if y[i-1] == 0:
            # per_day.append(per_day[i-2])
            per_day.append(0)
        else:
            per_day.append((y[i] - y[i-1]) / y[i-1] * 100)
    
    return per_day

def get_tasso_letalita(df):

    per_day = [0]
    y = np.array(df.deceduti)
    for i in range(1,len(y)):
        if y[i-1] == 0:
            # per_day.append(per_day[i-2])
            per_day.append(0)
        else:
            per_day.append((y[i] - y[i-1]) / y[i-1] * 100)
    
    return per_day

def get_tasso_guarigione(df):

    per_day = [0]
    y = np.array(df.dimessi_guariti)
    for i in range(1,len(y)):
        if y[i-1] == 0:
            # per_day.append(per_day[i-2])
            per_day.append(0)
        else:
            per_day.append((y[i] - y[i-1]) / y[i-1] * 100)
    
    return per_day

def plot_tasso(date, piemonte, lombardia, nazione, descrizione, axes):

    axes.plot(date, piemonte)
    axes.plot(lombardia)
    axes.plot(nazione)
    axes.axvline(LIMITAZIONI, color='r', ls='--', lw='1')
    axes.grid(True)
    # axes.set_xlabel('Data')
    axes.set_ylabel('%')
    axes.legend(['Piemonte', 'Lombardia', 'Italia'])
    axes.set_title(descrizione)

nazione = data.get_nazione()
nazione = nazione.query("data >= '2020-03-01'")
piemonte = data.get_regioni(name='Piemonte').query("data >= '2020-03-01'")
lombardia = data.get_regioni(name='Lombardia').query("data >= '2020-03-01'")

# Finding density
popolazione = pandas.read_csv(POPOLAZIONE)
abitanti = popolazione.query("regione == 'Piemonte'").popolazione.item()
superficie = popolazione.query("regione == 'Piemonte'").superficie.item()
densita = abitanti / superficie

# Get data
x1, y1 = get_attualmente_positivi(piemonte)
x2, y2 = get_attualmente_positivi(lombardia)
x3, y3 = get_attualmente_positivi(nazione)

# X axis
date = [i[5:-9] for i in piemonte.data]

### Piemonte
plt.figure()
plt.plot(date, y1, label = 'Real')

biased, unbiased = prepare_log_model(x1, y1)
y_unbiased = np.exp(unbiased[1]) * np.exp(unbiased[0] * x1)
legend = r'Today: ${:.3f}\cdot{:.3f}^x$'.format(unbiased[1], unbiased[0])
plt.plot(y_unbiased, label = legend)

biased, unbiased = prepare_log_model(x1, y1, 1)
y_unbiased = np.exp(unbiased[1]) * np.exp(unbiased[0] * x1)
legend = r'Yesterday: ${:.3f}\cdot{:.3f}^x$'.format(unbiased[1], unbiased[0])
plt.plot(y_unbiased, label = legend)

biased, unbiased = prepare_log_model(x1, y1, 2)
y_unbiased = np.exp(unbiased[1]) * np.exp(unbiased[0] * x1)
legend = r'Before yesterday: ${:.3f}\cdot{:.3f}^x$'.format(unbiased[1], unbiased[0])
plt.plot(y_unbiased, label = legend)

plt.grid(True)
plt.legend()
plt.axvline(LIMITAZIONI, color='r', ls='--', lw='1')
plt.title('Positivi - Piemonte')

### Lombardia
plt.figure()
plt.plot(date, y2, label = 'Real')

biased, unbiased = prepare_log_model(x2, y2)
y_unbiased = np.exp(unbiased[1]) * np.exp(unbiased[0] * x2)
legend = r'Today: ${:.3f}\cdot{:.3f}^x$'.format(unbiased[1], unbiased[0])
plt.plot(y_unbiased, label = legend)

biased, unbiased = prepare_log_model(x2, y2, 1)
y_unbiased = np.exp(unbiased[1]) * np.exp(unbiased[0] * x2)
legend = r'Yesterday: ${:.3f}\cdot{:.3f}^x$'.format(unbiased[1], unbiased[0])
plt.plot(y_unbiased, label = legend)

biased, unbiased = prepare_log_model(x2, y2, 2)
y_unbiased = np.exp(unbiased[1]) * np.exp(unbiased[0] * x2)
legend = r'Before yesterday: ${:.3f}\cdot{:.3f}^x$'.format(unbiased[1], unbiased[0])
plt.plot(y_unbiased, label = legend)

plt.grid(True)
plt.legend()
plt.axvline(LIMITAZIONI, color='r', ls='--', lw='1')
plt.title('Positivi - Lombardia')

### Confronto tamponi-contagi
fig = plt.figure()
plt.subplot(2, 2, 1)
plt.plot(date, np.array(piemonte.totale_casi))
plt.plot(np.array(piemonte.tamponi))
plt.axvline(LIMITAZIONI, color='r', ls='--', lw='1')
plt.grid(True)
plt.legend(['Positivi', 'Tamponi'])
plt.title('Piemonte')

plt.subplot(2, 2, 2)
plt.plot(date, np.array(lombardia.totale_casi))
plt.plot(np.array(lombardia.tamponi))
plt.axvline(LIMITAZIONI, color='r', ls='--', lw='1')
plt.grid(True)
plt.legend(['Positivi', 'Tamponi'])
plt.title('Lombardia')

plt.subplot(2, 2, 3)
plt.plot(date, np.array(nazione.totale_casi))
plt.plot(np.array(nazione.tamponi))
plt.axvline(LIMITAZIONI, color='r', ls='--', lw='1')
plt.grid(True)
plt.legend(['Positivi', 'Tamponi'])
plt.title('Italia')

plt.subplot(2, 2, 4)
plt.plot(date, np.array(piemonte.totale_casi) / np.array(piemonte.tamponi))
plt.plot(np.array(lombardia.totale_casi) / np.array(lombardia.tamponi))
plt.plot(np.array(nazione.totale_casi) / np.array(nazione.tamponi))
plt.axvline(LIMITAZIONI, color='r', ls='--', lw='1')
plt.grid(True)
plt.legend(['Piemonte', 'Lombardia', 'Italia'])
plt.title('Rapporto positivi-tamponi')

### Tassi del contagio
fig, ax = plt.subplots(2, 2)

# Tasso contagio
tasso_piemonte = get_tasso_contagio(piemonte)
tasso_lombardia = get_tasso_contagio(lombardia)
tasso_nazione = get_tasso_contagio(nazione)
plot_tasso(date, tasso_piemonte, tasso_lombardia, tasso_nazione, 'Tasso contagio', ax[0, 0])

# Tasso contagio mediato sui tamponi
tasso_piemonte = get_tasso_contagio_ponderato_tamponi(piemonte)
tasso_lombardia = get_tasso_contagio_ponderato_tamponi(lombardia)
tasso_nazione = get_tasso_contagio_ponderato_tamponi(nazione)
plot_tasso(date, tasso_piemonte, tasso_lombardia, tasso_nazione, 'Tasso contagio mediato sui tamponi', ax[0, 1])

# Tasso letalità
tasso_piemonte = get_tasso_letalita(piemonte)
tasso_lombardia = get_tasso_letalita(lombardia)
tasso_nazione = get_tasso_letalita(nazione)
plot_tasso(date, tasso_piemonte, tasso_lombardia, tasso_nazione, 'Tasso letalità', ax[1, 0])

# Tasso guarigioni
tasso_piemonte = get_tasso_guarigione(piemonte)
tasso_lombardia = get_tasso_guarigione(lombardia)
tasso_nazione = get_tasso_guarigione(nazione)
plot_tasso(date, tasso_piemonte, tasso_lombardia, tasso_nazione, 'Tasso guarigione', ax[1, 1])

# Show all plots
plt.show()
