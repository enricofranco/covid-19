from data_getter import data
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd


nazione = data.get_nazione()


nazione["data"] = nazione["data"].str.slice(0, -9)

nazione["data"] = pd.to_datetime(nazione["data"])

nazione = nazione.set_index("data")

plt.show(nazione["nuovi_positivi"].rolling(7).mean().plot(grid=True, legend="nuovi_positivi", title="Italia"))
plt.show(nazione["totale_positivi"].rolling(2).mean().plot(grid=True, legend="totale_positivi", title="Italia"))


print(nazione.corr())
