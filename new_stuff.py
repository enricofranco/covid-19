from data_getter import data
import matplotlib.pyplot as plt
from sklearn import linear_model
from sklearn.metrics import mean_squared_error
from sklearn.metrics import mean_absolute_error
from sklearn.neural_network import MLPRegressor
from sklearn.ensemble import AdaBoostRegressor
import numpy as np
import pandas

POPOLAZIONE = './data/popolazione.csv'

def prepare_data(df):

	x = [list(i) for i in zip(range(1, len(df.data)+1), df.tamponi)]
	y = [[i] for i in df.totale_attualmente_positivi]

	return x,y


nazione = data.get_nazione()
print(nazione.head())

date = [i[5:-9] for i in nazione.data]

tamponi_day = [nazione.tamponi[0]]
casi_day = [nazione.totale_attualmente_positivi[0]]

for i in range(1, len(nazione.tamponi)):
	tamponi_day.append(nazione.tamponi[i] - nazione.tamponi[i-1])
	casi_day.append(nazione.totale_attualmente_positivi[i] - nazione.totale_attualmente_positivi[i-1])

tamponi_day = np.array(tamponi_day)
casi_day = np.array(casi_day)


x = [list(i) for i in zip(range(1, len(nazione.data)+1), tamponi_day, nazione.tamponi)]
y = nazione.totale_attualmente_positivi

mlp = AdaBoostRegressor(loss='exponential', learning_rate=0.1, n_estimators=200)
mlp.fit(x,y)

print("\n--- MODEL SCORES ---")
print("MSE --> " + str(mean_squared_error(y, mlp.predict(x))))
print("MAE --> " + str(mean_absolute_error(y, mlp.predict(x))))

plt.plot(date, mlp.predict(x))
plt.plot(nazione.totale_attualmente_positivi)
plt.legend(['mlp', 'real'])
plt.grid(True)

plt.show()